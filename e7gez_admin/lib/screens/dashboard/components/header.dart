import 'package:e7gez_admin/constants.dart';
import 'package:e7gez_admin/controllers/MenuController.dart';
import 'package:e7gez_admin/global/environment.dart';
import 'package:e7gez_admin/models/user_model.dart';
import 'package:e7gez_admin/responsive.dart';
import 'package:e7gez_admin/screens/login/login_page.dart';
import 'package:e7gez_admin/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';


 class Header extends StatelessWidget {
  //const Header({Key? key,}): super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
        //key: context.read<MenuController>().scaffoldKey,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          if (!Responsive.isDesktop(context))
            IconButton(
              icon: const Icon(Icons.menu, color: primaryColor,),
              onPressed: context.read<MenuController>().controlMenu,
            ),
          if (!Responsive.isMobile(context))
            Text(
              "Dashboard",
              style: Theme.of(context).textTheme.headline6!.apply(color: primaryColor),
            ),
          if (!Responsive.isMobile(context))
            Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
            const ProfileCard()
        ],
    );
  }
}

class ProfileCard extends StatefulWidget{
const ProfileCard({Key? key}): super(key: key);
  @override
  ProfileCardState createState() => ProfileCardState();
}

class ProfileCardState extends State<ProfileCard>{
  ProfileCardState();
 User user = User();
 
  @override
  void initState(){
    super.initState();
    loadUser();
  }
  @override
  void setState(fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
 loadUser() async {
    const storage = FlutterSecureStorage();
    String? tempUser = await storage.read(key: 'user');
    if (!mounted) return;
    setState(() {
      user = userFromJson((tempUser)!);
    });
  }
 userImageDisplay(){
  if(user.imagePath != ""){
    return Image.network(
            "${Environment.apiUrl}/readFile/${user.imagePath}",
            height: 38,
          );
  }
  else{
    return Image.asset(
            "assets/images/blank-profile.png",
            height: 38,
          );
  }
 }
  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    return Container(
      margin: const EdgeInsets.only(left: defaultPadding/2),
      padding: EdgeInsets.symmetric(
        horizontal: defaultPadding /2,
        vertical: defaultPadding / (Responsive.isMobile(context) ? 2 : 1),
      ),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        border: Border.all(color: Colors.white10),
      ),
      child: Row(
        children: [
          userImageDisplay(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: defaultPadding / 4),
            child: Text(('${user.firstname} ${user.lastname}')),
            //child: Text(('${userFname} ${userLname}')),
          ),
          IconButton(
            tooltip: "logout",
            icon: const Icon(Icons.logout_rounded), 
            onPressed: () { 
              authService.logout();
              Navigator.pushNamed(context, LoginPage.routeName);
            },
          ),
        ],
      ),
    );
  }
}
//********************************************************************** */
//  class Header extends StatelessWidget {
//   // final String userFname;
//   //   final String userLname;
//   //   final String userImage;
//   // const Header({Key? key,required this.userFname, required this.userLname, required this.userImage,}): super(key: key);
// const Header({Key? key,}): super(key: key);
//   @override
//   Widget build(BuildContext context) {
    
//     return Row(
//        mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       children: [
//         if (!Responsive.isDesktop(context))
//           IconButton(
//             icon: const Icon(Icons.menu, color: primaryColor,),
//             onPressed: context.read<MenuController>().controlMenu,
//           ),
//         if (!Responsive.isMobile(context))
//           Text(
//             "Dashboard",
//             style: Theme.of(context).textTheme.headline6!.apply(color: primaryColor),
//           ),
//         if (!Responsive.isMobile(context))
//           Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
//         //Expanded(child: SearchField()),
//           //Expanded(child: ProfileCard(key:key,userFname:userFname,userLname:userLname,userImage:userImage))
//           Expanded(child: ProfileCard(key:key))
//       ],
//     );
//   }
// }

// class ProfileCard extends StatefulWidget{
//   // final String userFname;
//   //   final String userLname;
//   //   final String userImage;
//   //const ProfileCard({Key? key, required this.userFname, required this.userLname, required this.userImage}): super(key: key);
// const ProfileCard({Key? key}): super(key: key);
//   @override
//   //ProfileCardState createState() => ProfileCardState(userFname:userFname,userLname:userLname,userImage:userImage);
//   ProfileCardState createState() => ProfileCardState();
// }

// class ProfileCardState extends State<ProfileCard>{
//   // final String userFname;
//   //   final String userLname;
//   //   final String userImage;
//   //ProfileCardState({required this.userFname, required this.userLname, required this.userImage});
//   ProfileCardState();
//  User user = User();
 
//   @override
//   void initState(){
//     super.initState();
//     loadUser();
//   }
//   @override
//   void setState(fn) {
//     if(mounted) {
//       super.setState(fn);
//     }
//   }
// // @override
// //   void dispose() {

// //     super.dispose();
// //   }
//  loadUser() async {
//     const storage = FlutterSecureStorage();
//     String? tempUser = await storage.read(key: 'user');
//     if (!mounted) return;
//     setState(() {
//       user = userFromJson((tempUser)!);
//     });
//   }
//  userImageDisplay(){
//   if(user.imagePath != ""){
//     return Image.network(
//             "${Environment.apiUrl}/readFile/${user.imagePath}",
//             height: 38,
//           );
//   }
//   else{
//     return Image.asset(
//             "assets/images/blank-profile.png",
//             height: 38,
//           );
//   }
//  }
// //  userImageDisplay(){
// //   if(userImage != ""){
// //     return Image.network(
// //             "${Environment.apiUrl}/readFile/$userImage",
// //             height: 38,
// //           );
// //   }
// //   else{
// //     return Image.asset(
// //             "assets/images/blank-profile.png",
// //             height: 38,
// //           );
// //   }
// //  }
//   @override
//   Widget build(BuildContext context) {
//     final authService = Provider.of<AuthService>(context);
//     return Container(
//       margin: const EdgeInsets.only(left: defaultPadding/2),
//       padding: EdgeInsets.symmetric(
//         horizontal: defaultPadding /2,
//         vertical: defaultPadding / (Responsive.isMobile(context) ? 2 : 1),
//       ),
//       decoration: BoxDecoration(
//         color: secondaryColor,
//         borderRadius: const BorderRadius.all(Radius.circular(10)),
//         border: Border.all(color: Colors.white10),
//       ),
//       child: Row(
//         children: [
//           //userImageDisplay(),

//           Padding(
//             padding: const EdgeInsets.symmetric(horizontal: defaultPadding / 4),
//             child: Text(('${user.firstname} ${user.lastname}')),
//             //child: Text(('${userFname} ${userLname}')),
//           ),
//           IconButton(
//             tooltip: "logout",
//             icon: const Icon(Icons.logout_rounded), 
//             onPressed: () { 
//               authService.logout();
//               Navigator.pushNamed(context, LoginPage.routeName);
//             },
//           ),
//         ],
//       ),
//     );
//   }
// }


//********************************************************************** */


// class ProfileCard extends StatelessWidget {
//   const ProfileCard({
//     Key? key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     final authService = Provider.of<AuthService>(context);
//     //print("****************************************************************************************");
//     //print(loadUser(context).toString());
//     //User user = loadUser(context) as User;
//     return Container(
//       margin: const EdgeInsets.only(left: defaultPadding/2),
//       padding: EdgeInsets.symmetric(
//         horizontal: defaultPadding /2,
//         vertical: defaultPadding / (Responsive.isMobile(context) ? 2 : 1),
//         //horizontal: defaultPadding,
//         //vertical: defaultPadding / 2,
//       ),
//       decoration: BoxDecoration(
//         color: secondaryColor,
//         borderRadius: const BorderRadius.all(Radius.circular(10)),
//         border: Border.all(color: Colors.white10),
//       ),
//       child: Row(
//         children: [
//           Image.asset(
//             "assets/images/blank-profile.png",
//             height: 38,
//           ),
//           //if (!Responsive.isMobile(context))
//              Padding(
//               padding: const EdgeInsets.symmetric(horizontal: defaultPadding / 4),
//               child: Text(('${user.firstname}${user.lastname}')),
//             ),
//             IconButton(
//               tooltip: "logout",
//               icon: const Icon(Icons.logout_rounded), 
//               onPressed: () { 
//                 authService.logout();
//                 Navigator.pushNamed(context, LoginPage.routeName);
//               },
//             ),
//             // DropdownButton<Action>(
//             //   icon: const Icon(Icons.keyboard_arrow_down),
//             //   items: [
//             //   DropdownMenuItem(child:
//             //     IconButton(
//             //       tooltip: "logout",
//             //       icon: const Icon(Icons.logout_rounded), 
//             //       onPressed: () { 
//             //         authService.logout();
//             //         Navigator.pushNamed(context, LoginPage.routeName);
//             //       },
//             //     ),
//             //   ),
//             // ], onChanged: (Object? value) {  },),
           
//         ],
//       ),
//     );
//   }
// }

// class SearchField extends StatelessWidget {
//   const SearchField({
//     Key? key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return TextField(
//       decoration: InputDecoration(
//         hintText: "Search",
//         fillColor: secondaryColor,
//         filled: true,
//         border: const OutlineInputBorder(
//           borderSide: BorderSide.none,
//           borderRadius: BorderRadius.all(Radius.circular(10)),
//         ),
//         suffixIcon: InkWell(
//           onTap: () {},
//           child: Container(
//             padding: const EdgeInsets.all(defaultPadding * 0.75),
//             margin: const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
//             decoration: const BoxDecoration(
//               color: primaryColor,
//               borderRadius: BorderRadius.all(Radius.circular(10)),
//             ),
//             child: SvgPicture.asset("assets/icons/Search.svg"),
//           ),
//         ),
//       ),
//     );
//   }
// }