import 'package:e7gez_admin/constants.dart';
import 'package:e7gez_admin/models/reservation.model.dart';
import 'package:e7gez_admin/services/post_servise.dart';
import 'package:e7gez_admin/services/reservation_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:data_table_2/data_table_2.dart';


//  @override
//   Widget build(BuildContext context) {
//    return SafeArea(
//     child: SingleChildScrollView(
//         primary: false,
//         //padding: const EdgeInsets.all(defaultPadding),
//         child: Column(
//           children:    [
//              _recentReservations(context)
//           ],
//         ),
//       ),
//    );
//   }

class RecentReservations extends StatelessWidget {
  //const RecentReservations({Key? key,}) : super(key: key);
 late Future<List<Reservation>> _reservationList;


@override
  Widget build(BuildContext context) {
    final postService = Provider.of<ReservationService>(context);
    _reservationList = postService.getNewReservations();
    return FutureBuilder(
      future: _reservationList,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          return SafeArea( 
            child: Center(
              child: _getRecentReservations(context, snapshot.data)
            )
          );
        } else if (snapshot.hasError) {
          // If something went wrong
          return Text(snapshot.error.toString(),style: const TextStyle(color: Colors.black));
        }
        // While fetching, show a loading spinner.
        return const CircularProgressIndicator();
      }
    );
  }
  
  Widget _getRecentReservations(BuildContext context,List<Reservation> data) {
    return Container(
      padding: const EdgeInsets.only(top:defaultPadding/2,bottom: defaultPadding),
      decoration: const BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //Text("  Recent Reservations", style: Theme.of(context).textTheme.subtitle1),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("  Recent Reservations",style: Theme.of(context).textTheme.subtitle1),
              IconButton(icon: const Icon(Icons.refresh,color: Colors.white), onPressed:(){
                 Navigator.pop(context);
                 Navigator.pushNamed(context, '/dashbord');
              },)

            ],
          ),
          SizedBox(
            width: double.infinity,
            //ignore: sort_child_properties_last
            child: DataTable(
              dataTextStyle : const TextStyle(fontSize: 10),
              columnSpacing: defaultPadding/4,
              decoration:const BoxDecoration(
                color: testColor,
              ),
              // minWidth: 600,
              columns: const [
                DataColumn(label: Text("User Name",style: TextStyle(fontSize: 12))),
                DataColumn(label: Text("Visitor",style: TextStyle(fontSize: 12))),
                DataColumn(label: Text("Table",style: TextStyle(fontSize: 12))),
                DataColumn(label: Text("Status",style: TextStyle(fontSize: 12))),
                DataColumn(label: Text("Reserv Date",style: TextStyle(fontSize: 12))),
                DataColumn(label: Text("Action",style: TextStyle(fontSize: 12)))
              ],
              rows: List.generate(
                data.length,
                (index) => recentReservationDataRow(
                  data[index],index,context
                  ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  
}

DataRow recentReservationDataRow(Reservation reservInfo,int index,BuildContext context) {
  final Color dataColor=(index % 2 == 0)?Colors.black:Colors.white;
  final postService = Provider.of<ReservationService>(context);
  return DataRow(
    color: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
        if (index % 2 == 0) {
          return const Color.fromARGB(255, 255, 255, 255);//.withOpacity(0.3);
        }
        return const Color.fromARGB(255, 172, 170, 172).withOpacity(0.3);
      }
    ),
    cells: [
      DataCell(Text(reservInfo.userName!,style: TextStyle(color: dataColor))),
      DataCell(Text(reservInfo.numberOfGuests!.toString(),style: TextStyle(color: dataColor))),
      DataCell(Text(reservInfo.tableNumber!.toString(),style: TextStyle(color: dataColor))),
      DataCell(Text(reservInfo.reserveStatus!.toString(),style: TextStyle(color: dataColor))),
      DataCell(Text(reservInfo.dateOfReservation!.toString(),style: TextStyle(color: dataColor))),
      DataCell(
        PopupMenuButton<String>(
                initialValue: null,
                color:const Color.fromARGB(148, 12, 152, 105),
                icon: Icon(Icons.more_horiz,color: dataColor,),
                itemBuilder:(BuildContext context) => <PopupMenuEntry<String>>[
                   PopupMenuItem<String>(
                    value: 'approve',
                    child: const Text('Approve'),
                    onTap: () async {
                      FocusScope.of(context).unfocus();
                      await postService.updateStatus("APPROVED",reservInfo.reserveId!);
                    },
                  ),
                //  PopupMenuItem<String>(
                //     value: 'deactivate',//isActive? 'approve':'disapprove',APPROVED","DISAPPROVED
                //     child: const Text('Activate'),//isActive? const Text('Deactivate'):const Text('Activate'),
                //     onTap: (){},
                //   ),
                 PopupMenuItem<String>(
                    value: 'disapprove',
                    child: const Text('Disapprove'),
                    onTap: () async {
                      FocusScope.of(context).unfocus();
                      await postService.updateStatus("DISAPPROVED",reservInfo.reserveId!);
                    },
                  ),
              ]
           )
        //Text('tre',style: TextStyle(color: dataColor))
        ),
    ],
  );
}


// Widget _recentReservations(BuildContext context) {
  //   final postService = Provider.of<ReservationService>(context);
  //   _reservationList = postService.getNewReservations();
  //   return FutureBuilder(
  //     future: _reservationList,
  //     builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
  //       if (snapshot.hasData) {
  //         return SafeArea( 
  //           child: Center(
  //             child: _getRecentReservations(context, snapshot.data)
  //           )
  //         );
  //       } else if (snapshot.hasError) {
  //         // If something went wrong
  //         return Text(snapshot.error.toString());
  //       }

  //       // While fetching, show a loading spinner.
  //       return const CircularProgressIndicator();
  //     }
  //   );
  // }