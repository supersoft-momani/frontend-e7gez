import 'dart:io';

import 'package:e7gez_admin/constants.dart';
import 'package:e7gez_admin/controllers/MenuController.dart';
import 'package:e7gez_admin/global/environment.dart';
import 'package:e7gez_admin/models/page_like_subscribe.model.dart';
import 'package:e7gez_admin/models/post_info.model.dart';
import 'package:e7gez_admin/services/post_servise.dart';
import 'package:e7gez_admin/widgets/headers/mat_header.dart';
import 'package:e7gez_admin/widgets/side_menu/side_menu.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;




enum PopupMenuItems { itemOne, itemTwo, itemThree }

class PostTimelinePage extends StatelessWidget {
  static const routeName = '/posttimeline';

  const PostTimelinePage({Key? key}) : super(key: key);
  //const NewPostPage({required Key key}) : super(key: key);
@override
  Widget build(BuildContext context) {
    return Scaffold(
      key: context.read<MenuController>().scaffoldKey,
      drawer: const SideMenu(),
      appBar: const MatHeader(),
      body: SafeArea(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // if (Responsive.isDesktop(context))
            //   const Expanded(
            //     child: SideMenu(),
            //   ),
           // const Expanded(child:NarrowSideMenu()),
              Expanded(
              flex: 5,
              child: PostTimelineScreenPage()
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pop(context); Navigator.pushNamed(context, '/newpost');
        },
        backgroundColor: primaryColor,
        child: const Icon(Icons.add,color: Colors.white,),
      ),
    );
  }
}

class PostTimelineScreenPage extends StatelessWidget {
  //const NewPostScreenPage({Key? keyf}) : super(key: keyf);
  late Future<PageLikeSubscribeInfo> _pageLikeSubscribe;
 @override
  Widget build(BuildContext context) {
  
   return SafeArea(
    child: SingleChildScrollView(
        primary: false,
        padding: const EdgeInsets.all(defaultPadding),
        child: Column(
          children:    [
             _PageLikeSubscribe(context),
             const SizedBox(height: defaultPadding*2),
             PostTimelineSection(),
             const SizedBox(height: defaultPadding),
             //RecentReservations(),
          ],
        ),
      ),
   );
  }
Widget _PageLikeSubscribe(BuildContext context) {
    final postService = Provider.of<PostService>(context);
    _pageLikeSubscribe = postService.getPageLikeSubscribe();
    return FutureBuilder(
      future: _pageLikeSubscribe,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          return SafeArea( child: Center(
            child:Container(
              decoration: const BoxDecoration(
                color: Colors.white, 
                //rborder: Border.all(color: Colors.blueAccent,width: 1),
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                ),
              padding: const EdgeInsets.all(10),
              child: Column(
                children:[
                  CircleAvatar(
                    radius: 80.0,
                    backgroundImage: snapshot.data.image == "" ? const AssetImage("assets/images/logo.png") as ImageProvider :NetworkImage("${Environment.apiUrl}/readFile/"+(snapshot.data.image)),
                    ),
                  Text(snapshot.data.name,style: const TextStyle(color: primaryColor)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        height: 70,
                          decoration: const BoxDecoration(color: Colors.white),
                          child: Column(//Row(
                            children: <Widget>[
                              Container(
                                width: 70,
                                decoration: const BoxDecoration(color: Colors.white),
                                child: RawMaterialButton(
                                  shape: const RoundedRectangleBorder(side: BorderSide(color: Colors.white, style: BorderStyle.solid),borderRadius: BorderRadius.all(Radius.circular(5))),//const CircleBorder(side: BorderSide(color: Colors.white, style: BorderStyle.solid)),
                                  fillColor: Colors.blue,
                                  onPressed: () {},
                                  highlightElevation: 0,
                                  child: const Icon(Icons.thumb_up,color: Colors.white,size: 14,),
                                ),
                              ),
                              Text(snapshot.data.likes.toString(), style: const TextStyle(color: Colors.grey))
                              //Text('250', style: const TextStyle(color: Colors.grey))
                            ],
                          ),
                        ),
                      Container(
                        height: 70,
                        decoration: const BoxDecoration(color: Colors.white),
                        child: Column(//row(
                          children: <Widget>[
                            Container(
                              width: 70,
                              decoration: const BoxDecoration(color: Colors.white),
                              child: RawMaterialButton(
                                shape: const RoundedRectangleBorder(side: BorderSide(color: Colors.white, style: BorderStyle.solid),borderRadius: BorderRadius.all(Radius.circular(5))),//const CircleBorder(side: BorderSide(color: Colors.white, style: BorderStyle.solid)),
                                fillColor: Colors.red,
                                onPressed: () {},
                                highlightElevation: 8,
                                child: const Icon(Icons.thumb_down, color: Colors.white,size: 14,),
                              ),
                            ),
                            Text(snapshot.data.dislikes.toString(), style: const TextStyle(color: Colors.grey))
                            //Text('230', style: const TextStyle(color: Colors.grey))
                          ],
                        ),
                      ),
                      Container(
                        height: 70,
                          decoration: const BoxDecoration(color: Colors.white),
                          child: Column(//Row(
                            children: <Widget>[
                              Container(
                                width: 70,
                                decoration: const BoxDecoration(color: Colors.white),
                                child: RawMaterialButton(
                                  shape: const RoundedRectangleBorder(side: BorderSide(color: Colors.white, style: BorderStyle.solid),borderRadius: BorderRadius.all(Radius.circular(5))),//const CircleBorder(side: BorderSide(color: Colors.white, style: BorderStyle.solid)),
                                  fillColor: Colors.green,
                                  onPressed: () {},
                                  highlightElevation: 0,
                                  child: const Icon(Icons.subscriptions,color: Colors.white,size: 14,),
                                ),
                              ),
                              Text(snapshot.data.subscribes.toString(), style: const TextStyle(color: Colors.grey))
                              //Text('142', style: const TextStyle(color: Colors.grey))
                            ],
                          ),
                        ),
                    ],
                  ),
                ]
              )
              )
            )
          );
        } else if (snapshot.hasError) {
          // If something went wrong
          return Text(snapshot.error.toString());
        }

        // While fetching, show a loading spinner.
        return const CircularProgressIndicator();
      }
    );
  }
}
class PostTimelineSection extends StatefulWidget {
 //PostTimelinePage({this.storyController});

 //final StoryController storyController;

  _PostTimelineSectionState createState() => _PostTimelineSectionState();
}

class _PostTimelineSectionState extends State<PostTimelineSection> {
  int likes = 136;
  bool isLiked = false;
  bool hasStory = true;
  late Future<List<PostInfo>> _postList;
  
  @override
  void initState() {
    super.initState();
    
  }
  // void reactToPost() {
  //   setState(() {
  //     if (isLiked) {
  //       isLiked = false;
  //       likes--;
  //     } else {
  //       isLiked = true;
  //       likes++;
  //     }
  //   });
  //   print("Liked Post ? : $isLiked");
  // }
@override
  Widget build(BuildContext context) {
    final postService = Provider.of<PostService>(context);
    _postList = postService.getAllPosts();
    return FutureBuilder(
      future: _postList,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          return SafeArea( child: Center(
            child: Container(
              //constraints: const BoxConstraints(minWidth: 80, maxWidth: 600),
              //// padding: const EdgeInsets.symmetric(horizontal: 40),
              child:Column(children: _getPosts(snapshot.data))
             )
            )
          );
        } else if (snapshot.hasError) {
          // If something went wrong
          return Text(snapshot.error.toString());
        }

        // While fetching, show a loading spinner.
        return const CircularProgressIndicator();
      }
    );
  }
//   @override
//   Widget build(BuildContext context) {
//     // return Scaffold(
//     //   body: Center(
//     //           child: Container(
//     //       child: FloatingActionButton(
//     //         onPressed: () {
//     //           print('test');
//     //           Navigator.push(context, new MaterialPageRoute(
//     //             builder: (context) => UserStory(storyController: widget.storyController)
//     //           ));
//     //         },
//     //       )
//     //     ),
//     //   ),
//     // );
// return SafeArea( child: Center(
//       child: Container(
//         //constraints: const BoxConstraints(minWidth: 80, maxWidth: 600),
//        // padding: const EdgeInsets.symmetric(horizontal: 40),
//         child:Column(children: _getPosts())
//       )
//     )
//   );
//     // return Scaffold(
//     //   appBar: PreferredSize(
//     //     preferredSize: const Size.fromHeight(0),
//     //     child: AppBar(backgroundColor: Colors.white, elevation: 0),
//     //   ),
//     //   body: CustomScrollView(
//     //     slivers: <Widget>[
//     //       const SliverAppBar(
//     //         title: Text(
//     //           'facebook',
//     //           style: TextStyle(
//     //             fontWeight: FontWeight.w600,
//     //             fontSize: 26,
//     //             color: Color(0xff1777F0),
//     //           ),
//     //         ),
//     //         backgroundColor: Colors.white,
//     //         floating: true,
//     //         snap: true,
//     //         //actions: _getAppBarActions(),
//     //       ),
//     //       SliverList(
//     //           delegate: SliverChildListDelegate([
//     //         _getSeparator(5),
//     //         //_addPost(),
//     //         //_getSeparator(10),
//     //         //_getStoryContainer(),
//     //         Column(children: _getPosts())
//     //       ]))
//     //     ],
//     //   ),
//     // );
//   }
  
  // List<Widget> _getAppBarActions() {
  //   return [
  //     Container(
  //       child: IconButton(
  //         icon: Icon(Icons.search),
  //         color: Colors.black,
  //         disabledColor: Colors.black,
  //         splashColor: Theme.of(context).accentColor,
  //         onPressed: () {
  //           Navigator.push(
  //               context,
  //               new MaterialPageRoute(
  //                   builder: (context) =>
  //                       UserStory(storyController: widget.storyController)));
  //         },
  //       ),
  //       decoration: BoxDecoration(
  //         color: Theme.of(context).accentColor,
  //         shape: BoxShape.circle,
  //       ),
  //     ),
  //     Padding(
  //       padding: EdgeInsets.only(right: 5),
  //     ),
  //     Container(
  //       child: IconButton(
  //         icon: Icon(Icons.chat),
  //         color: Colors.black,
  //         disabledColor: Colors.black,
  //         splashColor: Theme.of(context).accentColor,
  //         onPressed: () {
  //           Navigator.push(
  //               context,
  //               new MaterialPageRoute(
  //                   builder: (context) =>
  //                       UserStory(storyController: widget.storyController)));
  //         },
  //       ),
  //       decoration: BoxDecoration(
  //         color: Theme.of(context).accentColor,
  //         shape: BoxShape.circle,
  //       ),
  //     ),
  //     Padding(
  //       padding: EdgeInsets.only(right: 10),
  //     ),
  //   ];
  // }

  Widget _getSeparator(double height) {
    return Container(
      decoration: BoxDecoration(color: Theme.of(context).dividerColor),
      constraints: BoxConstraints(maxHeight: height),
    );
  }
 
  List<Widget> _getPosts(List<PostInfo> data) {
    List<Widget> _posts = [];
    for (var i = 0; i < data.length; i++) {
      _posts.add(_getPost(data[i]));
      _posts.add(_getSeparator(5));
    }
    return _posts;
  }

  Widget _getPost(PostInfo info) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      child: Column(
        children: <Widget>[
          _getSeparator(10),
          _postHeader(info.postId!,info.createdBy!,info.dateCreated!,info.isActive!),
          _postBody(info.image!, info.text!),
          postLikesAndComments(info.likeCount!,info.dislikeCount!,info.commentsCount!),
          // const Divider(height: 1),
          // postOptions()
        ],
      ),
    );
  }
  Widget _postHeader(String postId, String createdBy,String createDate, bool isActive) {
    //String? selectedMenu; 
    final postService = Provider.of<PostService>(context);
    return Container(
      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              // Container(
              //   child: UserAvatar(hasStory: hasStory),
              //   padding: EdgeInsets.only(right: 10),
              // ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                 Text(createdBy,style: const TextStyle(fontWeight: FontWeight.w700, color: Colors.black)),
                  Row(
                    children: <Widget>[
                      Text(createDate,style: const TextStyle(color: Colors.grey, fontSize: 12)),
                      const Icon(Icons.language, size: 15, color: Colors.grey)
                    ],
                  )
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              //const Icon(Icons.star_border, color:  Colors.grey),
              PopupMenuButton<String>(
                initialValue: null,//selectedMenu,
                color:const Color.fromARGB(148, 12, 152, 105),
                // onSelected: (String item) {
                //   setState(() {
                //     selectedMenu = item;
                //   });
                // },
                itemBuilder:(BuildContext context) => <PopupMenuEntry<String>>[
                   PopupMenuItem<String>(
                    value: 'delete',
                    child: const Text('Delete'),
                    onTap: (){postService.delete(postId);},
                  ),
                 PopupMenuItem<String>(
                    value: isActive? 'deactivate':'activate',
                    child: isActive? const Text('Deactivate'):const Text('Activate'),
                    onTap: (){},
                  ),
                 PopupMenuItem<String>(
                    value: 'Item 3',
                    child: const Text('Item 3'),
                    onTap: (){},
                  ),
                ],
                icon: const Icon(Icons.more_horiz, color: Colors.grey),
                elevation: 16,
              ),
            ],
          ),
        ],
      ),
    );
    // Container(
    //   child: Text(
    //       "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."),
    //   decoration: BoxDecoration(color: Colors.green),
    //   padding: EdgeInsets.only(top: 5, bottom: 5),
    //   //padding: EdgeInsets.fromLTRB(0, 5, 0, 5)
    // ),
  }

// Future<ImageProvider> tryGetNetworkImage(String image) async{
//   var res =await http.get(Uri.parse("${Environment.apiUrl}/readFile/$image"));
//   if(res.statusCode != 400)
//   {return NetworkImage("${Environment.apiUrl}/readFile/$image") ;}
//   return const AssetImage('assetName');
// }
// ImageProvider tryGetNetworkImage(String image) {
//   try
//   {
//     var test = NetworkImage("${Environment.apiUrl}/readFile/$image");
//     if (test !=null) {
//       return test;
//     }//NetworkImage("${Environment.apiUrl}/readFile/$image");
//     else {
//       return const AssetImage("assets/images/logo.png");
//     }
//   } 
//   catch (e)
//   {
//     return const AssetImage("assets/images/logo.png");
//   }
// }
// ImageProvider tryGetNetworkImage(String image) {
//   return Image.network("${Environment.apiUrl}/readFile/$image",
// errorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
//         return const Text('Your error widget...');
//     },
//                 ) as ImageProvider;
// }

  Widget _postBody(String image, String text) {
        //debugPrint(image);
    return Column(
      //mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
        if(image=="") Container(constraints: const BoxConstraints(maxHeight: 10)),
        if(image!="")
        Container(
            constraints:const BoxConstraints(maxHeight: 350),
            child: FadeInImage(
              image: NetworkImage("${Environment.apiUrl}/readFile/$image"),
              placeholder: const AssetImage("assets/images/logo.png"),
              imageErrorBuilder:(context, error, stackTrace) {
                return Image.asset('assets/images/logo.png',fit: BoxFit.fill);
              },
              fit: BoxFit.fill,
            )
            // decoration:BoxDecoration(
            //   //color: primaryColor,
            //   image: DecorationImage(
            //     image: AssetImage("assets/images/logo.png"), //tryGetNetworkImage(image),//NetworkImage("${Environment.apiUrl}/readFile/$image"),
            //     fit: BoxFit.fill
            //     )
            //   ),
        ),
        Container(
          padding: const EdgeInsets.all(8.0),
          child: Text(text,style: const TextStyle(fontWeight: FontWeight.w500, color: Colors.black)),
        ),
            // Text(text,style: const TextStyle(fontWeight: FontWeight.w700, color: Colors.black)),
            ],
      
      
    );
  }

  Widget postLikesAndComments(int likeCount,int dislikeCount,int commentsCount) {
    return Container(
      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Row(
         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            height: 30,
              decoration: const BoxDecoration(color: Colors.white),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 30,
                    decoration: const BoxDecoration(color: Colors.white),
                    child: RawMaterialButton(
                      shape: const CircleBorder(side: BorderSide(color: Colors.white, style: BorderStyle.solid)),
                      fillColor: Colors.blue,
                      onPressed: () {},
                      highlightElevation: 0,
                      child: const Icon(Icons.thumb_up,color: Colors.white,size: 14,),
                    ),
                  ),
                  // Container(
                  //   // ignore: sort_child_properties_last
                  //   child: RawMaterialButton(
                  //       shape: const CircleBorder(
                  //           side: BorderSide(
                  //         color: Colors.white,
                  //       )),
                  //       fillColor: Colors.red,
                  //       onPressed: () {},
                  //       child: const Icon(
                  //         Icons.favorite,
                  //         color: Colors.white,
                  //         size: 14,
                  //       )),
                  //   width: 30,
                  //   color: Colors.white,
                  // ),
                  Text(likeCount.toString(), style: const TextStyle(color: Colors.grey))
                ],
              ),),
          Container(
            height: 30,
            decoration: const BoxDecoration(color: Colors.white),
            child: Row(
              children: <Widget>[
                Container(
                  width: 30,
                  decoration: const BoxDecoration(color: Colors.white),
                  child: RawMaterialButton(
                    shape: const CircleBorder(side: BorderSide(color: Colors.white, style: BorderStyle.solid)),
                    fillColor: Colors.red,
                    onPressed: () {},
                    highlightElevation: 8,
                    child: const Icon(Icons.thumb_down, color: Colors.white,size: 14,),
                  ),
                ),
                Text(dislikeCount.toString(), style: const TextStyle(color: Colors.grey))
              ],
            ),
          ),
          Container(
              child: Row(
            children: <Widget>[
              Text(
                commentsCount.toString(),
                style: TextStyle(color: Colors.grey),
              ),
              const Text(
                'Comments',//Comments
                style: TextStyle(color: Colors.grey),
              ),
            ],
          )
          )
        ],
       
      ),
    );
  }

  // Widget postOptions() {
  //   return Container(
  //     decoration: const BoxDecoration(
  //       color: Colors.white,
  //     ),
  //     padding: EdgeInsets.all(0),
  //     child: Row(
  //       mainAxisAlignment: MainAxisAlignment.spaceAround,
  //       children: <Widget>[
  //         Expanded(
  //             flex: 1,
  //             child: FlatButton.icon(
  //                 icon: Icon(Icons.thumb_up),
  //                 label: Text('Like'),
  //                 textColor: isLiked == true ? Colors.blue : Colors.grey,
  //                 onPressed: reactToPost)),
  //         Expanded(
  //             flex: 1,
  //             child: FlatButton.icon(
  //                 icon: Icon(Icons.comment),
  //                 label: Text('Comment'),
  //                 textColor: Colors.grey,
  //                 onPressed: () {
  //                   print('test');
  //                   // Navigator.push(
  //                   //     context,
  //                   //     new MaterialPageRoute(
  //                   //         builder: (context) => UserStory(
  //                   //             storyController: widget.storyController)));
  //                 })),
  //         Expanded(
  //             flex: 1,
  //             child: FlatButton.icon(
  //                 icon: Icon(Icons.share),
  //                 label: Text('Share'),
  //                 textColor: Colors.grey,
  //                 onPressed: () {})),
  //       ],
  //     ),
  //   );
  // }



  // Widget _getStoryContainer() {
  //   List<int> stories = [];
  //   stories.length = 10;
  //   return Container(
  //     child: ListView(
  //       children: <Widget>[
  //         Container(
  //           decoration: BoxDecoration(
  //             color: Colors.white,
  //           ),
  //           margin: EdgeInsets.only(right: 10),
  //           width: 2,
  //         ),
  //         _getYourStory(),
  //         Row(
  //           children: _getStoryThumbnails(),
  //           mainAxisSize: MainAxisSize.max,
  //         )
  //       ],
  //       scrollDirection: Axis.horizontal,
  //     ),
  //     decoration: BoxDecoration(color: Colors.white),
  //     height: 220,
  //     padding: EdgeInsets.fromLTRB(0, 12, 0, 12),
  //   );
  // }

  // Widget _getStoryThumbnail() {
  //   return InkWell(
  //     child: Container(
  //       child: InkWell(
  //         onTap: () {
  //           Navigator.push(
  //             context,
  //             new MaterialPageRoute(
  //               builder: (context) =>
  //               UserStory(storyController: widget.storyController)
  //             )
  //           );
  //         },
  //         child: Stack(
  //           children: <Widget>[
  //             Positioned(
  //               child: ClipRRect(
  //                 child: Image.network(userStoryCoverImage, fit: BoxFit.fill),
  //                 borderRadius: BorderRadius.all(Radius.circular(20)),
  //               ),
  //             ),
  //             // Positioned(
  //             //     //child: _friendAvatar(),
  //             //     child: UserAvatar(hasStory: hasStory),
  //             //     // height:40,
  //             //     // width: 40,
  //             //     left: 10,
  //             //     top: 5),
  //             // Positioned(
  //             //     child: Text('Hester \nVentura',
  //             //         style: TextStyle(color: Colors.white)),
  //             //     top: 130,
  //             //     left: 10)
  //           ],
  //           fit: StackFit.expand,
  //         ),
  //       ),
  //       margin: EdgeInsets.only(right: 10),
  //       width: 120,
  //       height: 220,
  //       decoration: BoxDecoration(color: Colors.white),
  //       padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
  //     ),
  //   );
  // }

  // Widget _getYourStory() {
  //   return Container(
  //     child: Stack(
  //       children: <Widget>[
  //         ClipRRect(
  //           child: Image.network(myStoryImage, fit: BoxFit.fill),
  //           borderRadius: BorderRadius.all(Radius.circular(20)),
  //         ),
  //         Positioned(
  //             child: FloatingActionButton(
  //               heroTag: "Addasdfdsfdsf",
  //               backgroundColor: Colors.white,
  //               child: Icon(
  //                 Icons.add,
  //                 color: Colors.blue,
  //                 size: 30,
  //               ),
  //               //onPressed: () {}
  //               onPressed: () {
  //                 print('pressed');
  //                 Navigator.push(
  //                     context,
  //                     new MaterialPageRoute(
  //                         builder: (context) => UserStory(
  //                             storyController: widget.storyController)));
  //               },
  //             ),
  //             left: 10,
  //             top: 5,
  //             height: 40,
  //             width: 40),
  //         Positioned(
  //             child:
  //                 Text('Add to Story', style: TextStyle(color: Colors.white)),
  //             top: 150,
  //             left: 10)
  //       ],
  //       fit: StackFit.expand,
  //     ),
  //     margin: EdgeInsets.only(right: 10),
  //     height: 220,
  //     width: 120,
  //     decoration: BoxDecoration(color: Colors.white),
  //     padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
  //   );
  // }

  // List<Widget> _getStoryThumbnails() {
  //   List<Widget> stories = [];
  //   for (var i = 0; i < 8; i++) {
  //     stories.add(_getStoryThumbnail());
  //   }
  //   return stories;
  // }

  // Widget _addPostHeader() {
  //   return Container(
  //     child: Row(
  //       children: <Widget>[
  //         Container(
  //             child: CircleAvatar(
  //               backgroundImage: new NetworkImage(userProfileImage),
  //             ),
  //             padding: EdgeInsets.only(right: 10)),
  //         Text(
  //           "What's on your Mind ?",
  //           style: TextStyle(color: Colors.black87),
  //         )
  //       ],
  //     ),
  //     padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
  //   );
  // }

  // Widget _addPostOptions() {
  //   return Container(
  //     child: Row(
  //       children: <Widget>[
  //         Expanded(
  //             child: FlatButton.icon(
  //                 icon: Icon(Icons.video_call, color: Colors.red),
  //                 label: Text('Live'),
  //                 textColor: Colors.grey,
  //                 onPressed: () {
  //                   print('test');
  //                   Navigator.push(
  //                       context,
  //                       new MaterialPageRoute(
  //                           builder: (context) => UserStory(
  //                               storyController: widget.storyController)));
  //                 }),
  //             flex: 1),
  //         Expanded(
  //             child: FlatButton.icon(
  //                 icon: Icon(Icons.photo, color: Colors.green),
  //                 label: Text('Photo'),
  //                 textColor: Colors.grey,
  //                 onPressed: () {
  //                   Navigator.push(
  //                       context,
  //                       new MaterialPageRoute(
  //                           builder: (context) => UserStory(
  //                               storyController: widget.storyController)));
  //                 }),
  //             flex: 1),
  //         Expanded(
  //           child: FlatButton.icon(
  //               icon: Icon(Icons.location_on, color: Colors.pink),
  //               label: Text('Check In'),
  //               textColor: Colors.grey,
  //               onPressed: () {
  //                 Navigator.push(
  //                     context,
  //                     new MaterialPageRoute(
  //                         builder: (context) => UserStory(
  //                             storyController: widget.storyController)));
  //               }),
  //           flex: 1,
  //         ),
  //       ],
  //       mainAxisAlignment: MainAxisAlignment.end,
  //     ),
  //   );
  // }

  // Widget _addPost() {
  //   return Container(
  //     child: Column(children: <Widget>[
  //       _addPostHeader(),
  //       Divider(),
  //       _addPostOptions(),
  //     ]),
  //     decoration: BoxDecoration(color: Colors.white),
  //   );
  // }

  //  Widget _getStories() {
  //   return Scaffold(
  //     appBar: AppBar(
  //       title: Text('test'),
  //       backgroundColor: Colors.white,
  //       leading: IconButton(
  //         icon: Icon(
  //           Icons.arrow_back_ios,
  //           color: Colors.black,
  //         ),
  //         onPressed: () {},
  //       ),
  //     ),
  //     body: Container(
  //       decoration: BoxDecoration(color: Colors.yellow),
  //       height: 300,
  //     ),
  //   );
  // }
}