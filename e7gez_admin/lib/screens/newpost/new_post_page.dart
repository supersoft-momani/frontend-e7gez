
// import 'dart:io';
import 'dart:io';
import 'dart:typed_data';

import 'package:e7gez_admin/constants.dart';
import 'package:e7gez_admin/controllers/MenuController.dart';
import 'package:e7gez_admin/helpers/show_alert.dart';
import 'package:e7gez_admin/models/user_model.dart';
import 'package:e7gez_admin/screens/dashboard/dashboard_screen.dart';
import 'package:e7gez_admin/services/post_servise.dart';
import 'package:e7gez_admin/widgets/button_sign.dart';
import 'package:e7gez_admin/widgets/headers/mat_header.dart';
import 'package:e7gez_admin/widgets/input.dart';
import 'package:e7gez_admin/widgets/side_menu/side_menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';
// import 'package:path_provider/path_provider.dart';

class NewPostPage extends StatelessWidget {
  static const routeName = '/newpost';

  const NewPostPage({Key? key}) : super(key: key);
  //const NewPostPage({required Key key}) : super(key: key);
@override
  Widget build(BuildContext context) {
    return Scaffold(
      key: context.read<MenuController>().scaffoldKey,
      drawer: const SideMenu(),
      appBar: const MatHeader(),
      body: SafeArea(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // if (Responsive.isDesktop(context))
            //   const Expanded(
            //     child: SideMenu(),
            //   ),
           // const Expanded(child:NarrowSideMenu()),
              Expanded(
              flex: 5,
              child: NewPostScreenPage()
            )
          ],
        ),
      ),
    );
  }
}

class NewPostScreenPage extends StatelessWidget {
  //const NewPostScreenPage({Key? keyf}) : super(key: keyf);
 @override
  Widget build(BuildContext context) {
   return SafeArea(
    child: SingleChildScrollView(
        primary: false,
        padding: const EdgeInsets.all(defaultPadding),
        child: Column(
          children:    [
            //  const Header(userFname: 'MOHAMMAD', userLname: 'ALOMARI', userImage: '',),
             // Header(),
            //  SizedBox(height: defaultPadding),
            //  Chart(),
             const SizedBox(height: defaultPadding*2),
             NewPostForm(),
             const SizedBox(height: defaultPadding),
             //RecentReservations(),
          ],
        ),
      ),
   );
  }
}
class NewPostForm extends StatefulWidget{
  //const NewPostForm({Key? keyv}) : super(key: keyv);
  @override
  NewPostFormState createState() => NewPostFormState();
}
class NewPostFormState extends State<NewPostForm> {
  final textController = TextEditingController();
  final passController = TextEditingController();
  //PickedFile? _pickedFile;
  File? _pickedFile;
  final ImagePicker _picker = ImagePicker();
  File? _imagePost;
  Uint8List? _imagePostWeb;
  String? _imagePostPath;
 @override
  void setState(fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  Future takePhoto(ImageSource source) async{
    final image = await _picker.pickImage(source: source);
    final imageTemp = File(image!.path);
    setState(() {
      _pickedFile = imageTemp;
    });
  }
   Future takeCameraPhoto() async{
    final image = await _picker.pickImage(source: ImageSource.camera);
    final imageTemp = File(image!.path);
    setState(() {
      _pickedFile = imageTemp;
    });
  }
  Future takeGalleryPhoto() async{
    final image = await _picker.pickImage(source: ImageSource.gallery);
    final imageTemp = File(image!.path);
    setState(() {
      _pickedFile = imageTemp;
    });
  }
  Future removeTakedPhoto() async{
    setState(() {
      _pickedFile = null;
    });
  }
  Future getCameraImage() async{
    final image = await ImagePicker().pickImage(source: ImageSource.camera);
    if(image==null) return;
    final imageTemp = File(image.path);
    if(this.mounted) {
      setState(() {
            this._imagePost=imageTemp;
            this._imagePostPath=image.path;
          });
    }
   
  }
  Future getGalleryImage() async{
    final image = await ImagePicker().pickImage(source: ImageSource.gallery);
    if(image==null) return;
    final imageTemp = File(image.path);
    if(this.mounted) {
      setState(() {
        this._imagePost=imageTemp;
        this._imagePostPath=image.path;
      });
    }
  }
  Future _getGalleryImage() async{
    final image = await ImagePicker().getImage(source: ImageSource.gallery,imageQuality:80);
    if(image==null) return;
    final imageTemp = await image.readAsBytes();
    if(this.mounted) {
      setState(() {
        this._imagePostWeb=imageTemp;
        this._imagePostPath=image.path;
      });
    }
  }
  Future removeImage() async{
    // final image = await ImagePicker().pickImage(source: ImageSource.gallery);
    // if(image==null) return;
    // final imageTemp = File(image.path);
    if (!mounted) return;
    setState(() {
      this._imagePost=null;
    });
  }
 User user = User();
  @override
  void initState(){
    super.initState();
    loadUser();
  }

  Future loadUser() async {
    const storage = FlutterSecureStorage();
    String? tempUser = await storage.read(key: 'user');
    if (!mounted) return;
    setState(() {
      user = userFromJson((tempUser)!);
    });
  }

  @override
  Widget build(BuildContext context) {
    final postService = Provider.of<PostService>(context);
    return SafeArea( child: Center(
      child: Container(
        constraints: const BoxConstraints(minWidth: 80, maxWidth: 600),
        padding: const EdgeInsets.symmetric(horizontal: 40),
        child: Column(children: [
          CircleAvatar(
            radius: 120.0,
            backgroundImage: _pickedFile == null ? const AssetImage("assets/images/logo.png") as ImageProvider : FileImage(File(_pickedFile!.path)),
          ),
          const SizedBox(height: 6),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const SizedBox(width: 10),
              CustomizedButton(icon: Icons.image_outlined, color: Colors.greenAccent[700]!, title: 'Gallery',width:135, onClick: takeGalleryPhoto,),
              const SizedBox(width: 10),
              CustomizedButton(icon: Icons.camera, color: Colors.greenAccent[700]!, title: 'Camera',width:135, onClick: takeCameraPhoto,),
            ],
          ),
          //Text(_pickedFile== null?"noooo":_pickedFile!.path),
          // _imagePostWeb != null 
          // ?Image.memory(_imagePostWeb!,width:250, height: 250, fit: BoxFit.cover,)
          // :SizedBox(width: 50,height: 16, child: Row(children: [
          //   Icon(Icons.image_outlined,color: Colors.greenAccent[700]!),
          // ])),
          // _imagePost != null 
          // ?Image.file(_imagePost!,width:250, height: 250, fit: BoxFit.cover,)
          // :SizedBox(width: 50,height: 16, child: Row(children: [
          //   Icon(Icons.image_outlined,color: Colors.greenAccent[700]!),
          // ])),
          // _imagePost != null 
          // ?Image.asset(_imagePostPath!,width:250, height: 250, fit: BoxFit.cover,)
          // :SizedBox(width: 50,height: 16, child: Row(children: [
          //   Icon(Icons.image_outlined,color: Colors.greenAccent[700]!),
          // ])),
          //if(_imagePost != null) CustomizedButton(icon: Icons.remove_circle, color: const Color.fromARGB(255, 200, 0, 0), title: 'Remove image', onClick: removeImage,),

          if(_pickedFile != null) CustomizedButton(icon: Icons.remove_circle, color: const Color.fromARGB(255, 200, 0, 0), title: 'Remove image', onClick: removeTakedPhoto,),
          const SizedBox(height: 16),
          RichInput(
            icon: Icons.post_add_outlined,
            placeholder: 'Post heare',
            maxLenght: 400,
            controller: textController,
          ),
         
          const SizedBox( height: 16),
          //CustomizedButton(icon: Icons.image_outlined, color: Colors.greenAccent[700]!, title: 'Pick from gallery', onClick: _getGalleryImage,),
          // CustomizedButton(icon: Icons.image_outlined, color: Colors.greenAccent[700]!, title: 'Pick from gallery', onClick: takeGalleryPhoto,),
          // const SizedBox(height: 6),
          //CustomizedButton(icon: Icons.camera, color: Colors.greenAccent[700]!, title: 'Pick from camera', onClick: getCameraImage,),
          // CustomizedButton(icon: Icons.camera, color: Colors.greenAccent[700]!, title: 'Pick from camera', onClick: takeCameraPhoto,),
          // const SizedBox(height: 6),
          CustomizedButton(icon: Icons.save, color: Colors.greenAccent[700]!, title: 'Create Post', onClick: () async {
                    FocusScope.of(context).unfocus();
                    final createOK = await postService.formDataCreate(textController.text, _pickedFile!=null?_pickedFile!.path:null);
                    if (createOK.statusCode==200 || createOK.statusCode==201) {
                      // ignore: use_build_context_synchronously
                      //Navigator.pushReplacementNamed(context, DashboardScreen.routeName);

                      // ignore: use_build_context_synchronously
                       Navigator.pop(context); 
                       // ignore: use_build_context_synchronously
                       Navigator.pushNamed(context, '/posttimeline');
                    } else {
                      // ignore: use_build_context_synchronously
                      showAlert(context, 'Post Incorrect',
                          'Check your credentials and try again');
                    }
                  },
          ),
          const SizedBox(height: 6),
          // SignButton(
          //   label: 'Create Post',
          //   press: postService.loading
          //       ? null
          //       : () async {
          //           FocusScope.of(context).unfocus();
          //           final createOK = await postService.createPost(textController.text, _imagePostWeb);
          //           if (createOK) {
          //             // ignore: use_build_context_synchronously
          //             Navigator.pushReplacementNamed(
          //                 context, DashboardScreen.routeName);
          //           } else {
          //             // ignore: use_build_context_synchronously
          //             showAlert(context, 'Post Incorrect',
          //                 'Check your credentials and try again');
          //           }
          //         },
          // ),
          // const SizedBox(height: 6),
          CustomizedButton(icon: Icons.cancel, color: Colors.red, title: 'Cancel', onClick: (){ Navigator.pop(context); Navigator.pushNamed(context, '/posttimeline');},),
          // SignButton(
          //   label: 'Cancel',
          //   press:(){ Navigator.pop(context); Navigator.pushNamed(context, '/posttimeline');}
          // ),
        ]),
      ),
    ),
    );
  }
}
Widget CustomizedButton({
  required String title,
  required IconData icon,
  required VoidCallback onClick,
  required Color color,
  double? width = 280,
}){
  return SizedBox(
    width: width,
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
        elevation: 5,
        primary: color,
      ),
      onPressed: onClick,
      child: Row(children: [
        Icon(icon),
        const SizedBox(width: 20),
        Text(title)
      ],
     ),
    ),
  );
}
 


// class CreatProfile extends StatefulWidget {
//   const CreatProfile({Key? key}) : super(key: key);
  
//   @override
//   _CreatProfileState createState() => _CreatProfileState();
// }

// class _CreatProfileState extends State<CreatProfile> {
 
//   @override
//   Widget build(BuildContext context) {
//     return const Scaffold();}}

// class NewPostScreenPage extends StatelessWidget {
//   const NewPostScreenPage({Key? key}) : super(key: key);
//  @override
//   Widget build(BuildContext context) {
//    return SafeArea(
//     child: SingleChildScrollView(
//         primary: false,
//         padding: const EdgeInsets.all(defaultPadding),
//         child: Column(
//           children:  const [
//              Header(),
//             //  SizedBox(height: defaultPadding),
//             //  Chart(),
//              SizedBox(height: defaultPadding),
//              CardsField(),
//              SizedBox(height: defaultPadding),
//              //RecentReservations(),
//           ],
//         ),
//       ),
//    );
//   }
// }
 





//  Widget imageProfile() {
//     return Center(
//       child: Stack(children: <Widget>[
//         CircleAvatar(
//           radius: 80.0,
//           backgroundImage: _imageFile == null
//               ? AssetImage("assets/profile.jpeg")
//               : FileImage(File(_imageFile.path)),
//         ),
//         Positioned(
//           bottom: 20.0,
//           right: 20.0,
//           child: InkWell(
//             onTap: () {
//               showModalBottomSheet(
//                 context: context,
//                 builder: ((builder) => bottomSheet()),
//               );
//             },
//             child: Icon(
//               Icons.camera_alt,
//               color: Colors.teal,
//               size: 28.0,
//             ),
//           ),
//         ),
//       ]),
//     );
//   }



// @override
//   void dispose() {
//     textController.dispose();
//     super.dispose();
//   }

// @override
//   Widget build(BuildContext context) {
//     //final authService = Provider.of<AuthService>(context);
//     return Scaffold(
//       body: SingleChildScrollView(
//         physics: ClampingScrollPhysics(parent: NeverScrollableScrollPhysics()),
//         child: Center(
//       child: Container(
//         constraints: const BoxConstraints(minWidth: 180, maxWidth: 600),
//         //height: size.height,
//         padding: const EdgeInsets.symmetric(horizontal: 40),
//         child: Column(children: [
//           _imagePost != null 
//           ?Image.file(_imagePost!,width:250, height: 250, fit: BoxFit.cover,)
//           :SizedBox(width: 50,height: 16, child: Row(children: [
//             Icon(Icons.image_outlined,color: Colors.greenAccent[700]!),
//           ])),
//           if(_imagePost != null) CustomizedButton(icon: Icons.remove_circle, color: const Color.fromARGB(255, 200, 0, 0), title: 'Remove image', onClick: removeImage,),
//           RichInput(
//             icon: Icons.post_add_outlined,
//             placeholder: 'Post heare',
//             maxLenght: 400,
//             controller: textController,
//           ),
//           const SizedBox( height: 16),
//           CustomizedButton(icon: Icons.image_outlined, color: Colors.greenAccent[700]!, title: 'Pick from gallery', onClick: getGalleryImage,),
//           const SizedBox(height: 16),
//           CustomizedButton(icon: Icons.camera, color: Colors.greenAccent[700]!, title: 'Pick from camera', onClick: getCameraImage,),
//           const SizedBox(height: 16),
//           // SignButton(
//           //   label: 'Create Post',
//           //   press: authService.loading
//           //       ? null
//           //       : () async {
//           //           FocusScope.of(context).unfocus();
//           //           final loginOK = await authService.login(
//           //               textController.text.trim(), passController.text);
//           //           if (loginOK) {
//           //             // ignore: use_build_context_synchronously
//           //             Navigator.pushReplacementNamed(
//           //                 context, DashboardScreen.routeName);
//           //           } else {
//           //             // ignore: use_build_context_synchronously
//           //             showAlert(context, 'Login Incorrect',
//           //                 'Check your credentials and try again');
//           //           }
//           //         },
//           // ),
//         ]),
//       ),
//     ),
//       ),
//     );
//   }

 // @override
  // Widget build(BuildContext context) {
  //   final authService = Provider.of<AuthService>(context);
  //   //final socketService = Provider.of<SocketService>(context);
  //   return Center(
  //     child: Container(
  //       constraints: const BoxConstraints(minWidth: 180, maxWidth: 600),
  //       padding: const EdgeInsets.symmetric(horizontal: 40),
  //       child: Column(children: [
  //         _imagePost != null 
  //         ?Image.file(_imagePost!,width:250, height: 250, fit: BoxFit.cover,)
  //         :SizedBox(width: 50,height: 16, child: Row(children: [
  //           Icon(Icons.image_outlined,color: Colors.greenAccent[700]!),
  //         ])),
  //         if(_imagePost != null) CustomizedButton(icon: Icons.remove_circle, color: const Color.fromARGB(255, 200, 0, 0), title: 'Remove image', onClick: removeImage,),
  //         // Input(
  //         //   icon: Icons.post_add_outlined,
  //         //   placeholder: 'Post heare',
  //         //   maxLenght: 32,
  //         //   controller: textController,
  //         // ),
  //         // RichInput(
  //         //   icon: Icons.post_add_outlined,
  //         //   placeholder: 'Post heare',
  //         //   maxLenght: 400,
  //         //   controller: textController,
  //         // ),
  //         Container(
  //       alignment: Alignment.center,
  //       padding: const EdgeInsets.all(6),
  //       child: TextFormField(
  //         controller: textController,
  //         decoration: const InputDecoration(border: OutlineInputBorder()),
        
  //       ),
  //     ),
  //         const SizedBox(
  //           height: 16,
  //         ),
  //         CustomizedButton(icon: Icons.image_outlined, color: Colors.greenAccent[700]!, title: 'Pick from gallery', onClick: getGalleryImage,),
  //         const SizedBox(height: 16,),
  //         CustomizedButton(icon: Icons.camera, color: Colors.greenAccent[700]!, title: 'Pick from camera', onClick: getCameraImage,),
  //         const SizedBox(height: 16,),
  //         // Input(
  //         //   icon: Icons.lock_outlined,
  //         //   placeholder: 'Password',
  //         //   maxLenght: 32,
  //         //   controller: passController,
  //         //   hidden: true,
  //         // ),
  //         // const SizedBox(
  //         //   height: 16,
  //         // ),
  //         SignButton(
  //           label: 'Create Post',
  //           press: authService.loading
  //               ? null
  //               : () async {
  //                   FocusScope.of(context).unfocus();
  //                   final loginOK = await authService.login(
  //                       textController.text.trim(), passController.text);
  //                   if (loginOK) {
  //                     //conectar a socketserver
  //                     //socketService.connect("noroom");
  //                     //navegar a la pantalla de usuarios
                      
  //                     // ignore: use_build_context_synchronously
  //                     Navigator.pushReplacementNamed(
  //                         context, DashboardScreen.routeName);
  //                   } else {
  //                     // ignore: use_build_context_synchronously
  //                     showAlert(context, 'Login Incorrect',
  //                         'Check your credentials and try again');
  //                   }
  //                 },
  //         ),
  //       ]),
  //     ),
  //   );
  // }