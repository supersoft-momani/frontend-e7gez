class PostInfo {
  final String? postId,image, text, vendorId,createdBy, dateCreated;
  final int? likeCount, dislikeCount, commentsCount;
final bool? isActive;
  PostInfo({
    this.postId,
    this.text,
    this.image,
    this.vendorId,
    this.createdBy,
    this.dateCreated,
    this.likeCount,
    this.dislikeCount,
    this.commentsCount,
    this.isActive
  });

  static PostInfo fromJson(Map<String, dynamic> map) {
    return PostInfo(
      postId: map['postId'].toString(),
      text: map['text'].toString(),
      image: map['image'].toString(),
      vendorId: map['vendorId'].toString(),
      createdBy: map['createdBy'].toString(),
      dateCreated: map['dateCreated'].toString(),
      likeCount: map['likeCount'],//int.tryParse(map['likeCount']),
      dislikeCount: map['dislikeCount'],//int.tryParse(map['dislikeCount']),
      commentsCount: map['commentsCount'],//int.tryParse(map['commentsCount'])
      isActive: map['isActive']
    );
  }
}
