import 'dart:convert';

import 'package:e7gez_admin/models/user_model.dart';


LoginResponse loginResponseFromJson(String str) =>
    LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    required this.user,
    required this.token,
    required this.expiresIn
  });

  User user;
  String token;
  int expiresIn;

  factory LoginResponse.fromJson(Map<String, dynamic> json)  {
    print(json["user"]);
    return LoginResponse(
        user: userFromLoinJson(json["user"]),
        token: json["token"],
        expiresIn: json["expiresIn"]
      );}

  Map<String, dynamic> toJson() => {
        "user": userToJson(user),
        "token": token,
        "expiresIn":expiresIn
      };
}