import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));
User userFromLoinJson(Map<String, dynamic> str) => User.fromLoinJson(str);
String userToJson(User data) => json.encode(User.toJson(data));

class User {
  // User({
  //   required this.id,
  //   required this.firstname,
  //   required this.lastname,
  //   required this.email,
  //   required this.password,
  //   required this.dateCreated,
  //   required this.phoneNumber,
  //   required this.role,
  //   required this.token,
  //   required this.isActive,
  //   required this.point,
  //   required this.createdBy,
  //   required this.imagePath,
  //   required this.userNumber
  // });
User({
  this.id="",
   this.firstname="",
   this.lastname="",
   this.email="",
   this.password="",
   this.dateCreated="",
   this.phoneNumber="",
   this.role="",
   this.token="",
   this.vendorId="",
   this.isActive=false,
   this.point="",
   this.createdBy="",
   this.imagePath="",
   this.userNumber=0,
});

  // bool online;
  // String name;
  // String email;
  // String uid;
  // String time;

  String firstname;
  String lastname;
  String email;
  String password;
  String dateCreated;
  String phoneNumber;
  String role;
  String token;
  String vendorId;
  bool isActive;
  String point;
  String createdBy;
  String imagePath;
  int userNumber;
  String id;

factory User.fromLoinJson(Map<String, dynamic> json) => User(
        id: json["id"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        email: json["email"],
        password:"",//json["password"],
        dateCreated: json["dateCreated"],
        phoneNumber: json["phoneNumber"],
        role: json["role"],
        token: json["token"],
        vendorId: json["vendorId"],
        isActive: true,//json["isActive"] as bool,
        point: "",//json["point"],
        createdBy: "",//json["createdBy"],
        imagePath:  json["imagePath"],
        userNumber: 0,//json["userNumber"],
      );
  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: json["id"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        email: json["email"],
        password:json["password"],
        dateCreated: json["dateCreated"],
        phoneNumber: json["phoneNumber"],
        role: json["role"],
        token: json["token"],
        vendorId: json["vendorId"],
        isActive: json["isActive"],
        point: json["point"],
        createdBy: json["createdBy"],
        imagePath: json["imagePath"],
        userNumber: json["userNumber"],
      );
    }

  static Map<String, dynamic> toJson(User data) => {
      "id":data.id,
      "firstname":data.firstname,
      "lastname":data.lastname,
      "email":data.email,
      "password":data.password,
      "dateCreated":data.dateCreated,
      "phoneNumber":data.phoneNumber,
      "role":data.role,
      "token":data.token,
      "vendorId": data.vendorId,
      "isActive":data.isActive,
      "point":data.point,
      "createdBy":data.createdBy,
      "imagePath":data.imagePath,
      "userNumber":data.userNumber
  };
}



// factory User.fromLoinJson(Map<String, dynamic> json) => User(
//         id: json["id"],
//         firstname: json["firstname"],
//         lastname: json["lastname"],
//         email: json["email"],
//         password: "",
//         dateCreated: json["dateCreated"],
//         phoneNumber: json["phoneNumber"],
//         nonHashedPassword: "",
//         role: json["role"],
//         token: json["token"],
//         isActive: true,
//         point: "",
//         createdBy: "",
//         imagePath: json["imagePath"],
//         userNumber: 0,
//       );