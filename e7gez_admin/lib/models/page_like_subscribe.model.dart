class PageLikeSubscribeInfo {
  final String? name,image;
  final int? likes, dislikes, subscribes;

  PageLikeSubscribeInfo({
    this.name,
    this.image,
    this.likes,
    this.dislikes,
    this.subscribes
  });

  static PageLikeSubscribeInfo fromJson(Map<String, dynamic> map) {
    return PageLikeSubscribeInfo(
      name: map['name'].toString(),
      image: map['image'].toString(),
      likes: map['likes'],
      dislikes: map['dislikes'],
      subscribes: map['subscribes']
    );
  }
}
