class Reservation {
  final String? reserveId,clientId,userName, phoneNumber, reserveStatus,vendorId;
  final int?  numberOfGuests, tableNumber,reserveNumber;
  final ReservClient? userId;
  final DateTime? dateCreated,dateOfReservation;
  final bool? outSide,isActive;
  Reservation({
    this.clientId,
    this.userName,
    this.phoneNumber,
    this.reserveStatus,

    
    this.reserveId,
    this.vendorId,
    this.numberOfGuests,
    this.tableNumber,
    this.reserveNumber,

    this.userId,

    this.dateCreated,
    this.dateOfReservation,
    this.outSide,
    this.isActive
  });
   static Reservation fromJson(Map<String, dynamic> map) {
    Map<String,dynamic> mapClient=map["userId"];
    var client = ReservClient.fromJson(mapClient);
    return Reservation(
      userName:'${client.firstname} ${client.lastname}',
      phoneNumber:client.phoneNumber.toString(),
      reserveStatus:map['reserveStatus'].toString(),
 vendorId: map['vendorId'].toString(),
      clientId:client.userId,
      reserveId: map['_id'],
     
      numberOfGuests: map['numberOfGuests'],
      tableNumber: map['tableNumber'],
      reserveNumber: map['reserveNumber'],

      dateCreated: DateTime.parse(map['dateCreated']),
      dateOfReservation: DateTime.parse(map['dateOfReservation']),

      outSide: map['outSide'],
      isActive: map['isActive']
    );
  }


//NOTE:
//reserveStatus:"NEW","APPROVED","DISAPPROVED","CANCELLED","ENDED"

  // static Reservation fromJson(Map<String, dynamic> map) {
  //   return new Reservation(
  //   title: map['title'].toString(),
  //   value: int.tryParse(map['value']),
  //   svgSrc: map['svgSrc'].toString(),
  //   color: int.tryParse(map['color']),
  //   percentage: int.tryParse(map['percentage'])
  // );
  // }
}
class ReservClient {
  final String? firstname, lastname, phoneNumber,userId;
  ReservClient({
    this.firstname,
    this.lastname,
    this.phoneNumber,
    this.userId,
  });
static ReservClient fromJson(Map<String, dynamic> map) {
    return ReservClient(
      firstname: map['firstname'].toString(),
      lastname:map['lastname'].toString(),
      phoneNumber:map['phoneNumber'].toString(),
      userId:map['userId'].toString()
    );
  }
}

// List<Reservation> demoRecentReservations= [
//   Reservation(
//     reserveId:1,
//     restaurantId:1,
//     userName:"mohammad alomari",
//     phone:"0792877948",
//     visitorNumber:5,
//     tableNumber:1,
//     reserveStatus:"NEW",
//     creatDate:DateTime.now(),
//     reservDate:DateTime.now()
//   ),
//   Reservation(
//     reserveId:2,
//     restaurantId:1,
//     userName:"mohammad alomari",
//     phone:"0792877948",
//     visitorNumber:8,
//     tableNumber:2,
//     reserveStatus:"NEW",
//     creatDate:DateTime.now(),
//     reservDate:DateTime.now()
//   ),
//   Reservation(
//     reserveId:3,
//     restaurantId:1,
//     userName:"mohammad alomari",
//     phone:"0792877948",
//     visitorNumber:5,
//     tableNumber:1,
//     reserveStatus:"NEW",
//     creatDate:DateTime.now(),
//     reservDate:DateTime.now()
//   ),
//   Reservation(
//     reserveId:4,
//     restaurantId:1,
//     userName:"mohammad alomari",
//     phone:"0792877948",
//     visitorNumber:8,
//     tableNumber:2,
//     reserveStatus:"NEW",
//     creatDate:DateTime.now(),
//     reservDate:DateTime.now()
//   )
//  ]; 