import 'package:e7gez_admin/screens/dashboard/dashboard_screen.dart';
import 'package:e7gez_admin/screens/loading/loading_page.dart';
import 'package:e7gez_admin/screens/login/login_page.dart';
import 'package:e7gez_admin/screens/newpost/new_post_page.dart';
import 'package:e7gez_admin/screens/post_timline/post_timeline_page.dart';
import 'package:e7gez_admin/screens/second/second_screen.dart';
import 'package:flutter/material.dart';

final Map<String, Widget Function(BuildContext)> routes = {
  LoginPage.routeName: (_) => const LoginPage(),
  // DashboardScreen.routeName: (_) => const DashboardScreen(key: Key("__DashboardScreen__")),
  DashboardScreen.routeName: (_) =>  const DashboardScreen(),
  SecondScreen.routeName: (_) => SecondScreen(),
  LoadingPage.routeName: (_) => const LoadingPage(),
  NewPostPage.routeName: (_) =>  const NewPostPage(),
  PostTimelinePage.routeName: (_) =>  const PostTimelinePage(),
  // NewPostPage.routeName: (_) => const NewPostPage(key: Key("__NewPostPage__")),
  // RegisterPage.routeName: (_) => RegisterPage(),
  // 
  // NewroomPage.routeName: (_) => NewroomPage(),
  // ChatPage.routeName: (_) => ChatPage(),
};