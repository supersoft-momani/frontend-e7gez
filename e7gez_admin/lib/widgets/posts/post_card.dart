// import 'package:e7gez_admin/constants.dart';
// import 'package:e7gez_admin/models/post_info.model.dart';
// import 'package:flutter/material.dart';

// class PostCard extends StatelessWidget {
//   const PostCard({
//     Key? key,
//     required this.info,
//   }) : super(key: key);

//   final PostInfo info;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       decoration: const BoxDecoration(
//         color: secondaryColor,
//         borderRadius: BorderRadius.all(Radius.circular(10)),
//         // boxShadow: [BoxShadow(
//         //             color: Color(0xFFC7C7C7),//.withOpacity(0.5),
//         //             spreadRadius: 5,
//         //             blurRadius: 7,
//         //             offset: Offset(0, 3),
//         //           )],
//           ),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           Container(
//             padding: const EdgeInsets.all(5),
//             child: Row(
//             //mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               Card(
//                 color:cardBackColor,
//                 child: Container(
//                   padding: const EdgeInsets.all(defaultPadding * 0.25),
//                   height: 50,
//                   width: 50,
//                   decoration: BoxDecoration(
//                     color: Color(info.color!).withOpacity(0.05),
//                     borderRadius: const BorderRadius.all(Radius.circular(5)),
//                   ),
//                   child: SvgPicture.asset(
//                     info.svgSrc!,
//                     color: Color(info.color!),
//                   ),
//                 )
//               ),
//               Text(
//                 "${info.value}\n${info.unit}",
//                 style: const TextStyle(fontSize: 14),
//                 overflow: TextOverflow.ellipsis,
//               ),
//               // FittedBox(
//               //   fit: BoxFit.fitWidth, 
//               //   child: Text(
//               //   "${info.value} ${info.unit}",
//               //    style: const TextStyle(fontSize: 15),//Theme.of(context)
//               //   //     .textTheme
//               //   //     .caption!
//               //   //     .copyWith(color: Colors.white70),
//               //   ), 
//               // ),
//             ],
//           ),
//             //const Icon(Icons.person, size: 24, color:Colors.blueAccent),
//           ),
//           Container(
//             decoration: BoxDecoration(
//               color: Color(info.color!).withOpacity(0.8),
//               borderRadius: const BorderRadius.only(bottomRight: Radius.circular(10), bottomLeft: Radius.circular(10))
//             ),
//             padding: const EdgeInsets.all(8),
//             child: 
//             Column(
//               children:[
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     FittedBox(
//                     fit: BoxFit.fitWidth, 
//                     child: Text(
//                             info.title!,
//                             maxLines: 1,
//                             style: const TextStyle(fontSize: 16),
//                             overflow: TextOverflow.ellipsis,
//                           )
//                     ),
//                   ],
//                 )
//               ]
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }