import 'package:e7gez_admin/constants.dart';
import 'package:e7gez_admin/controllers/MenuController.dart';
import 'package:e7gez_admin/global/environment.dart';
import 'package:e7gez_admin/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({
    Key? key,
  }) : super(key: key);
 
  @override
  Widget build(BuildContext context) {
    return Drawer(
      //  color:const Color(0xFF0C9869),
      child: ListView(
        children: [
          // DrawerHeader(
          //   child: UserHeader()//Image.asset("assets/images/logo.png"),
          // ),
          UserHeader(),
          // const Card(
          //   child: ListTile(
          //     leading: FlutterLogo(),
          //     title: Text('One-line with leading widget'),
          //   ),
          // ),
          
          const Divider(color: cardBackColor),
          Card(
            color: cardBackColor,
            child: DrawerListTile(
              title: "Dashboard",
              iconSrc: Icons.dashboard_outlined,
              // svgSrc: "assets/icons/menu_dashbord.svg",
              press: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/dashbord');
              },
            )
          ),
          Card(
            color:cardBackColor,
            child: DrawerListTile(
              title: "Transaction",
              iconSrc: Icons.book_online_outlined,
              // svgSrc: "assets/icons/menu_tran.svg",
              press: () {Navigator.pop(context); Navigator.pushNamed(context, '/second');},
            )
          ),
          Card(
            color:cardBackColor,
            child: DrawerListTile(
              title: "New Post",
              iconSrc: Icons.post_add_outlined,
              // svgSrc: "assets/icons/menu_task.svg",
              press: () {Navigator.pop(context); Navigator.pushNamed(context, '/newpost');},
            )
          ),
          Card(
            color:cardBackColor,
            child:  DrawerListTile(
              title: "Time Line",
              iconSrc: Icons.timeline_outlined,
              // svgSrc: "assets/icons/menu_doc.svg",
              press: () {Navigator.pop(context); Navigator.pushNamed(context, '/posttimeline');},
            )
          ),
          // Card(
          //   color:cardBackColor,
          //   child: DrawerListTile(
          //     title: "Store",
          //     svgSrc: "assets/icons/menu_store.svg",
          //     press: () {},
          //   )
          // ),
          Card(
            color:cardBackColor,
            child: DrawerListTile(
              title: "Notification",
              iconSrc: Icons.notifications_outlined,
              // svgSrc: "assets/icons/menu_notification.svg",
              press: () {},
            )
          ),
          Card(
            color:cardBackColor,
            child:  DrawerListTile(
              title: "Profile",
              iconSrc: Icons.person_outline,
              //svgSrc: "assets/icons/menu_profile.svg",
              press: () {},
            )
          ),
          Card(
            color:cardBackColor,
            child: DrawerListTile(
              title: "Settings",
              iconSrc: Icons.settings_outlined,
              //svgSrc: "assets/icons/menu_setting.svg",
              press: () {},
            )
          ),
          Card(
            color:Colors.white.withOpacity(0),
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: context.read<MenuController>().closeControlMenu,
              child: const ListTile(
                leading: Icon(Icons.arrow_back, color: Colors.white),
              ),
              //title: const Text('Close'),
            ),
          ),
        ],
      ),
    );
  }
}
class UserHeader extends StatefulWidget{
  @override
  UserHeaderState createState() => UserHeaderState();
}
class UserHeaderState extends State<UserHeader> {
User user = User();
    @override
    void initState(){
      super.initState();
      loadUser();
    }
    @override
    void setState(fn) {
      if(mounted) {
        super.setState(fn);
      }
    }
    loadUser() async {
        const storage = FlutterSecureStorage();
        String? tempUser = await storage.read(key: 'user');
        setState(() {
          user = userFromJson((tempUser)!);
        });
      }
    userImageDisplay(){
      if(user.imagePath != ""){
        //return Image.network("${Environment.apiUrl}/readFile/${user.imagePath}",height: 38);
        return NetworkImage("${Environment.apiUrl}/readFile/${user.imagePath}");
      }
      else{
        return const AssetImage( "assets/images/blank-profile.png");
      }
    }
    
      @override
      Widget build(BuildContext context) {
        return CircleAvatar(
            radius: 90.0,
            backgroundColor: Colors.white,
            //backgroundImage: _pickedFile == null ? const AssetImage("assets/images/logo.png") as ImageProvider : FileImage(File(_pickedFile!.path)),
            backgroundImage: userImageDisplay()//AssetImage("assets/images/logo.png"),
          );
      }
}
class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key? key,
    // For selecting those three line once press "Command+D"
    required this.title,
    required this.iconSrc,
    required this.press,
  }) : super(key: key);

  final String title;
  final VoidCallback press;
  final IconData iconSrc;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      horizontalTitleGap: 0.0,
      //decoration:const BoxDecoration(boxShadow: [BoxShadow(color: Color.fromARGB(255, 255, 0, 0))])
      leading: Icon(iconSrc,size: 18,color: cardTextColor,),
  
      title: Text(
        title,
        style: const TextStyle(color: cardTextColor),
      ),
    );
  }
}

//use svg
// class DrawerListTile extends StatelessWidget {
//   const DrawerListTile({
//     Key? key,
//     // For selecting those three line once press "Command+D"
//     required this.title,
//     required this.svgSrc,
//     required this.press,
//   }) : super(key: key);

//   final String title, svgSrc;
//   final VoidCallback press;

//   @override
//   Widget build(BuildContext context) {
//     return ListTile(
//       onTap: press,
//       horizontalTitleGap: 0.0,
//       //decoration:const BoxDecoration(boxShadow: [BoxShadow(color: Color.fromARGB(255, 255, 0, 0))])
//       leading:const Icon(Icons.dashboard_outlined,size: 18,color: cardTextColor,),
//       // leading: SvgPicture.asset(
//       //   svgSrc,
//       //   color: cardTextColor,
//       //   height: 16,
//       // ),
//       title: Text(
//         title,
//         style: const TextStyle(color: cardTextColor),
//       ),
//     );
//   }
// }


// class MenuItem{
//  late String title, svgSrc;
//  late VoidCallback press;
//  MenuItem(this.title, this.svgSrc,  press);
// }
// final List<MenuItem> menuItems = <MenuItem>[
//   new MenuItem{
//     title: "Settings",
//             svgSrc: "assets/icons/menu_setting.svg",
//             press: () {},
//   }
// ];