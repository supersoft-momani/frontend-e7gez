import 'package:flutter/material.dart';

class MenuController extends ChangeNotifier {
 GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

 GlobalKey<ScaffoldState> _scaffoldKeyPost = GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKeyPost => _scaffoldKeyPost;
  void controlMenu() {
    if (!_scaffoldKey.currentState!.isDrawerOpen) {
      _scaffoldKey.currentState!.openDrawer();
    }
    if (!_scaffoldKeyPost.currentState!.isDrawerOpen) {
      _scaffoldKeyPost.currentState!.openDrawer();
    }
  }
  void controlMenuPost() {
    if (!_scaffoldKeyPost.currentState!.isDrawerOpen) {
      _scaffoldKeyPost.currentState!.openDrawer();
    }
  }
   void closeControlMenu() {
    if (_scaffoldKey.currentState!.isDrawerOpen) {
      _scaffoldKey.currentState!.closeDrawer();
    }
  }
}
