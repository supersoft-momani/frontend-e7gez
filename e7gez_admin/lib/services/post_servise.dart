


import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';
import 'package:e7gez_admin/models/page_like_subscribe.model.dart';
import 'package:e7gez_admin/models/post_info.model.dart';
import 'package:http/http.dart';
import 'package:e7gez_admin/global/environment.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';


class PostService with ChangeNotifier{
 
  bool _loading = false;

  final _storage = const FlutterSecureStorage();

  bool get loading => _loading;
  set loading(value) {
    _loading = value;
    notifyListeners();
   }
  
  Future<http.StreamedResponse> formDataCreate(String text, String? image) async{
    String? token = await _storage.read(key: 'token');
    String? vendorId= await _storage.read(key: 'vendorId');
    debugPrint(image);
    var request =http.MultipartRequest('POST',(Uri.parse('${Environment.apiUrl}/post/create')));
    
    request.fields['text']=text;
    request.fields['vendorId']=vendorId!;

    if(image!=null) request.files.add(await http.MultipartFile.fromPath('thumbnail',image));
    request.headers.addAll({
      // 'Content-Typeb': 'application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization':'Bearer $token',
      'x-token': token!
    });
    var response = request.send();
    return response;
  }


Future<List<PostInfo>> getAllPosts() async {
  String? vendorId= await _storage.read(key: 'vendorId');
  String? token = await _storage.read(key: 'token');
 final response = await http.get(Uri.parse('${Environment.apiUrl}/post/getAll/$vendorId'),headers: {'Authorization':'Bearer $token',});
  
  List<PostInfo> getList =<PostInfo>[];
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      List<dynamic> values=<dynamic>[];
      values = json.decode(response.body);
      if(values.isNotEmpty){
        for(int i=0;i<values.length;i++){
          if(values[i]!=null){
            Map<String,dynamic> map=values[i];
            getList.add(PostInfo.fromJson(map));
          }
        }
      }
      return getList;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

 Future<PageLikeSubscribeInfo> getPageLikeSubscribe() async {
  String? vendorId= await _storage.read(key: 'vendorId');
  String? token = await _storage.read(key: 'token');
  final response = await http.get(Uri.parse('${Environment.apiUrl}/item/getPageLikeSubscribe/$vendorId'),
  headers: {'Authorization':'Bearer $token',});

    if (response.statusCode == 200) {
      dynamic values;
      values = json.decode(response.body);
      Map<String,dynamic> map=values;
      return PageLikeSubscribeInfo.fromJson(map);
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load data');
    }
  }
  Future delete(String postId) async{
    String? token = await _storage.read(key: 'token');
    final response = await http.delete(Uri.parse('${Environment.apiUrl}/post/delete/$postId'),headers: {'Authorization':'Bearer $token',});
    if (response.statusCode == 200) {
      Fluttertoast.showToast(
              msg: response.body,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: const Color.fromARGB(255, 94, 224, 77),
              textColor: Colors.white,
              fontSize: 16.0
          );
    } else {
      Fluttertoast.showToast(
              msg: response.body,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
    }
  }

  Future uploadImage(String image, String? token) async {
      // var map = new Map<String, dynamic>();
      // map['thumbnail'] = image;
      //var imageFile = File(image);
      final request = http.MultipartRequest('POST',Uri.parse('${Environment.apiUrl}/thumbnail-upload'));
      await http.MultipartFile.fromPath(
      'thumbnail',
      image,
      // filename:'image_${DateTime.now()}.jpg',
      //contentType: MediaType('application', 'x-tar'),
    );
      //var response = 
      await request.send().then((result) async {
        http.Response.fromStream(result)
        .then((response) {
          if (response.statusCode == 200)
          {
            Fluttertoast.showToast(
              msg: response.body,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: const Color.fromARGB(255, 94, 224, 77),
              textColor: Colors.white,
              fontSize: 16.0
            );
            print("Uploaded! ");
            print('response.body '+response.body);
          }
          return response.body;
       });})
       // ignore: invalid_return_type_for_catch_error
       .catchError((err) => Fluttertoast.showToast(
              msg: err.toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          ))
       .whenComplete((){});
       return null;
      // final response = await http.post(Uri.parse('${Environment.apiUrl}/thumbnail-upload'),
      //   body: map,
      //   headers: {'Authorization':'Bearer $token','x-token': token!});
      //   var mapRes = new Map<String, dynamic>();
    //     if (response.statusCode == 200) {
    //   Fluttertoast.showToast(
    //           msg: response.toString(),
    //           toastLength: Toast.LENGTH_SHORT,
    //           gravity: ToastGravity.CENTER,
    //           timeInSecForIosWeb: 1,
    //           backgroundColor: const Color.fromARGB(255, 94, 224, 77),
    //           textColor: Colors.white,
    //           fontSize: 16.0
    //       );
    //   return true;
    // } else {
    //   Fluttertoast.showToast(
    //           msg: response.toString(),
    //           toastLength: Toast.LENGTH_SHORT,
    //           gravity: ToastGravity.CENTER,
    //           timeInSecForIosWeb: 1,
    //           backgroundColor: Colors.red,
    //           textColor: Colors.white,
    //           fontSize: 16.0
    //       );
    //   return false;
    // }
    
   }
   
  // Future createPost(String text, Uint8List? image) async {
  //   loading = true;
  //   //final _storage = FlutterSecureStorage();
  //   String? token = await _storage.read(key: 'token');
  //   String? vendorId= await _storage.read(key: 'vendorId');
  //   String? resUplode="";

  //   if(image!=null){
  //     print("Upload! ");
  //     //List<int> imageFile = image.readAsBytesSync();
  //     String base64Image = base64Encode(image);
  //     resUplode ='data:image/jpeg;base64,$base64Image';
  //     //resUplode=base64Image;
  //      //resUplode = await uploadImage(image,token!);
  //     // print(resUplode);
  //   } 
  //   else{
  //     resUplode = null;
  //   }
    
    
    
  //   final request = {"postobj":{'text': text,'image':resUplode, 'vendorId': vendorId}};
   
  //   final response = await http.post(Uri.parse('${Environment.apiUrl}/post/create'),
  //       body: jsonEncode(request),
  //       headers: {'Content-Type': 'application/json','Authorization':'Bearer $token','x-token': token!});

  //   loading = false;

  //   if (response.statusCode == 201) {
      
  //     //final String data = response.body;
  //     Fluttertoast.showToast(
  //             msg: response.body,
  //             toastLength: Toast.LENGTH_SHORT,
  //             gravity: ToastGravity.CENTER,
  //             timeInSecForIosWeb: 1,
  //             backgroundColor: const Color.fromARGB(255, 94, 224, 77),
  //             textColor: Colors.white,
  //             fontSize: 16.0
  //         );
  //     return true;
  //   } else {
  //     Fluttertoast.showToast(
  //             msg: response.body,
  //             toastLength: Toast.LENGTH_SHORT,
  //             gravity: ToastGravity.CENTER,
  //             timeInSecForIosWeb: 1,
  //             backgroundColor: Colors.red,
  //             textColor: Colors.white,
  //             fontSize: 16.0
  //         );
  //     return false;
  //   }
  // }
  
}