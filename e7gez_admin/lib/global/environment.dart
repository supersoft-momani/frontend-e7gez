import 'package:e7gez_admin/constants.dart';

class Environment {
  static String apiUrl = isDevlopment
      ? 'http://localhost:3000/api/v1'
      : 'https://backend-e7gez.onrender.com/api/v1';

  static String socketUrl = isDevlopment
      ? 'http://localhost:3000/api/v1'
      : 'https://backend-e7gez.onrender.com/api/v1';
}